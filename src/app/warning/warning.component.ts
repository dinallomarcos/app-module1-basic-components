import { Component } from '@angular/core';

@Component({
selector: 'app-warning',
template: `<p>Warning!!! You are in danger!!!</p>`,
styles: [`p {
        padding: 40px;
        background-color: mistyrose;
        border: 1px solid red;
    }`]
})
export class WarningAletComponent {

}
